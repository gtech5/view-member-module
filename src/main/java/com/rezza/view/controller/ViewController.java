package com.rezza.view.controller;

import com.rezza.view.db.model.TbMember;
import com.rezza.view.process.ViewProcess;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Objects;

@RestController
@RequestMapping("/v1")
public class ViewController {

    @Autowired
    private ViewProcess viewProcess;

    @ApiOperation(value = "Register user")
    @PostMapping(value = "/view", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity view(@RequestBody RequestModel payloads) {
        TbMember tbMember = viewProcess.view(payloads).get(0);

        HashMap mapResult = new HashMap();
        mapResult.put("firstName", tbMember.getTbmFirstName());
        mapResult.put("lastName", tbMember.getTbmLastName());
        mapResult.put("gender", tbMember.getTbmGender());
        mapResult.put("mobileNumber", tbMember.getTbmMobilePhone());
        mapResult.put("email", tbMember.getTbmEmail());

        HashMap mapMessage = new HashMap();
        mapMessage.put("status", !Objects.isNull(tbMember) ? "success":"failed");
        mapMessage.put("data",mapResult);

        return new ResponseEntity(mapMessage , HttpStatus.OK);
    }
}