package com.rezza.view.controller;

import lombok.Data;

@Data
public class RequestModel {
    private String phone;
    private String email;
}
