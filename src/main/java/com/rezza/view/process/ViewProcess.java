package com.rezza.view.process;

import com.rezza.view.controller.RequestModel;
import com.rezza.view.db.mapper.TbMemberMapper;
import com.rezza.view.db.model.TbMember;
import com.rezza.view.db.model.TbMemberExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViewProcess {
    @Autowired
    private TbMemberMapper memberMapper;

    public List<TbMember> view(RequestModel requestModel) {
        TbMemberExample tbMemberExample = new TbMemberExample();
        tbMemberExample.createCriteria()
                .andTbmMobilePhoneEqualTo(requestModel.getPhone())
                .andTbmEmailEqualTo(requestModel.getEmail());

        return memberMapper.selectByExample(tbMemberExample);
    }
}